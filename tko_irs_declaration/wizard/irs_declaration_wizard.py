from odoo import models, fields, api
from datetime import datetime
YEARS_VALS = [(i,i) for i in range(2016, datetime.utcnow().year + 1)]


class IrsDeclarationWizard(models.TransientModel):
    _name = 'irs.declaration.wizard'
    _description = 'IRS Declaration Wizard'

    partner_id = fields.Many2one('res.partner', 'Partner')
    year = fields.Selection(YEARS_VALS, 'Year')

    @api.model
    def default_get(self, fields):
        context = self.env.context
        result = super(IrsDeclarationWizard, self).default_get(fields)
        if context.get('active_id'):
            result['partner_id'] = context.get('active_id')
        return result


    def print_report(self):
        self.partner_id.write({'irs_year' : int(self.year)})
        return self.env.ref('tko_irs_declaration.irs_declaration').report_action(self.partner_id)