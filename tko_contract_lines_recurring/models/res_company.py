from odoo import models, fields, api


class ResCompany(models.Model):
    _inherit = 'res.company'

    confirm_recurring_invoice = fields.Boolean(default=True, string='Confirm Recurring Invoice on Creation')