# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'


    # invoice_is_email = fields.Boolean(string='Send Email', related='company_id.invoice_is_email', readonly=False)
    confirm_recurring_invoice = fields.Boolean(string='Confirm Recurring Invoice on Creation', related='company_id.confirm_recurring_invoice', readonly=False)

