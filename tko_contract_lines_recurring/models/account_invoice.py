from odoo import models, fields, api

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def create(self, vals):
        record = super(AccountInvoice, self).create(vals)
        ### Set invoice in contracts
        if record.contract_id and record.type == 'out_invoice':
            for line in record.invoice_line_ids:
                contract_lines = record.contract_id.recurring_invoice_line_ids.filtered(
                    lambda l: l.product_id == line.product_id)
                if len(contract_lines) == 1:
                    if record not in contract_lines.invoice_ids:
                        contract_lines.write({'invoice_ids': [(4, record.id)]})
        return record

    @api.multi
    def write(self, vals):
        result = super(AccountInvoice, self).write(vals)
        ### Set invoice in contracts
        for record in self:
            if record.contract_id and record.type == 'out_invoice':
                for line in record.invoice_line_ids:
                    contract_lines = record.contract_id.recurring_invoice_line_ids.filtered(lambda l: l.product_id == line.product_id)
                    if len(contract_lines) == 1:
                        if record not in contract_lines.invoice_ids:
                            contract_lines.write({'invoice_ids' : [(4,record.id)]})
        return result


