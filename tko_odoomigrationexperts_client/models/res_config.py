from odoo import models, fields, api
from os import urandom

class OdooMigrationExpertsConfig(models.TransientModel):
    _inherit = 'res.config.settings'


    migrationexpoerts_token = fields.Char('OdooMigrationExperts TOKEN')

    def generate_token(self):
        self.set_values()
        return True

    @api.multi
    def set_values(self):
        super(OdooMigrationExpertsConfig, self).set_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        token = ICPSudo.get_param('tko_odoomigrationexperts_client.token')
        if not token:
            token = urandom(16).hex()
        ICPSudo.set_param("tko_odoomigrationexperts_client.token", token)

    @api.model
    def get_values(self):
        res = super(OdooMigrationExpertsConfig, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        res.update(
            migrationexpoerts_token=ICPSudo.get_param('tko_odoomigrationexperts_client.token'),
        )
        return res
