# © 2019 TKOpen <https://tkopen.com>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    'name': 'FaturAqui with Sale',
    'summary': 'Updates FaturAqui invoices from sale orders',
    'description': '',
    'author': 'TKOpen',
    'category': 'Sales',
    'license': 'LGPL-3',
    'website': 'https://tkopen.com',
    'version': '12.0.0.0.1',
    'sequence': 10,
    'depends': [
        'tko_faturaqui_client',
        'sale',
    ],
    'data': [],
    'init_xml': [],
    'update_xml': [],
    'css': [],
    'demo_xml': [],
    'test': [],
    'external_dependencies': {
        'python': [],
        'bin': [],
    },
    'images': [],
    'installable': True,
    'application': False,
    'auto_install': True,
}
