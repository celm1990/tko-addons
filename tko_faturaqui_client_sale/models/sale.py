from odoo import api, models


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        if self.product_id and self.product_id.tax0_reason_id:
            res['tax0_reason_id'] = self.product_id.tax0_reason_id.id
        return res
